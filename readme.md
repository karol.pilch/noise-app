NoiseApp
========

This is a small project I wrote to get some hands-on practice in DSP ahead of
some job interviews.

Requirements
------------

This repo contains projects to open it in:
* Xcode 11 on MacOS 10.15
* Visual Studio 2017 on Windows 10

To compile and link successfully, you'll need
[PortAudio](http://www.portaudio.com/) compiled and available for linker, with
the `portaudio.h` available in your include path.

I sincerely hope that the source code works in other environments since I 
strive to write portable code, however I make no promises. It works OK for me 
on MacOS.

What this app does
------------------

* Generates noise
* Puts it through band pass filter
* Measures the output volume
* Listens to some keyboard commands and applies them to signal processors

Usage
-----

1. Run the app and press 'h'. On Windows, you might need to press Return
   afterwards.
2. Press the keys listed in help. The '_' and '*' symbols in the prompt
   indicate the current volume.

Notes
-----

I wrote this quickly and with the goal of getting out something that works.
It's not very polished, but represents quite well how I think and work under
tight time constraints. 

I realise that there's a lot that can be improved:
* The signal is altered without smoothing and the jumps in volume and filter
  frequency are perceivable
* The single-char input isn't working on Windows
* The code for band-pass frequency changes is clumsy
* Noise generator needs proper random seed and is likely not very performant
* Thread safety between control and audio can likely be optimised
* The command output is very crude and confirmation text disappears too quickly
* Probably a good few more!

I hope you enjoy the visit! Comment and get in touch if you have any questions
/ sugestions.
