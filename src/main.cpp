//
//  main.cpp
//  NoiseApp
//
//  Created by Karol Pilch on 24/10/2019.
//  Copyright © 2019 Karol Pilch. All rights reserved.
//

#include <atomic>
#include <chrono>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <thread>

#include "audio/AudioEngine.hpp"
#include "audio/AudioEngineTypes.hpp"
#include "audio/dsp/BandPassFilter.hpp"
#include "audio/dsp/NoiseGenerator.hpp"
#include "audio/dsp/VolumeMeter.hpp"
#include "audio/dsp/SineGenerator.hpp"
#include "ControlProcessor.hpp"

using namespace na;

int main(int argc, const char * argv[])
{
    auto noiseGenerator = std::make_shared<NoiseGenerator>();
    auto filter = std::make_shared<BandPassFilter>();
    auto volumeMeter = std::make_shared<VolumeMeter>();
    try
    {
        AudioEngine engine;

        engine.addProcessor(noiseGenerator);
        engine.addProcessor(filter);
        engine.addProcessor(volumeMeter);

        ControlProcessor control {"vol: __________"};

        control.registerCommand('i', "Volume up", [&noiseGenerator]
        {
            noiseGenerator->changeVolume(0.05f);
            std::stringstream result;
            result << "Volume: " << std::setprecision(3) << std::setw(5) << noiseGenerator->getVolume();
            return result.str();
        });

        control.registerCommand('k', "Volume down", [&noiseGenerator]
        {
            noiseGenerator->changeVolume(-0.05f);
            std::stringstream result;
            result << "Volume: " << std::setprecision(3) << std::setw(5) << noiseGenerator->getVolume();
            return result.str();
        });

        control.registerCommand('p', "Play / Pause", [&engine]
        {
            engine.toggle();
            return engine.isPlaying() ? "Play" : "Pause";
        });

        control.registerCommand('f', "Enable / disable band pass filter", [&filter]
        {
            filter->toggleEnabled();
            std::stringstream result;
            result << "Filter " << (filter->isEnabled() ? "enabled" : "disabled");
            return result.str();
        });

        control.registerCommand('z', "Filter's low frequency down", [&filter]
        {
            filter->changeParams(-1, 0);
            return filter->getParamsDescription();
        });

        control.registerCommand('Z', "Filter's low frequency up", [&filter]
        {
            filter->changeParams(1, 0);
            return filter->getParamsDescription();
        });

        control.registerCommand('x', "Filter's high frequency down", [&filter]
        {
            filter->changeParams(0, -1);
            return filter->getParamsDescription();
        });

        control.registerCommand('X', "Filter's high frequency up", [&filter]
        {
            filter->changeParams(0, 1);
            return filter->getParamsDescription();
        });

        control.printHelp();

        std::atomic_bool runUpdates{true};
        std::thread promptUpdateThread {[&]
        {
            while(runUpdates)
            {
                std::stringstream prompt;
                prompt << "vol: ";

                int bars {volumeMeter->getVolumeBars(10)};
                for (int i {1}; i <= 10; ++i)
                {
                    prompt << (i > bars ? '_' : '*');
                }
#ifndef DEBUG_TIME
                control.setPrompt(prompt.str());
#endif
                std::this_thread::sleep_for(std::chrono::milliseconds {250});
            }
        }};

        promptUpdateThread.detach();

        // register commands
        control.handle();

        // Terminate and wait for update thread to finish before we leave the scope.
        runUpdates = false;
        if (promptUpdateThread.joinable())
        {
            promptUpdateThread.join();
        }
    }
    catch (const AudioEngine::PortAudioError& e)
    {
        std::cerr << "Error in audio engine: \"" << e.what() << "\"" << std::endl;
        return 1;
    }
	return 0;
}
