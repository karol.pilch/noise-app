//
//  ControlProcessor.hpp
//  NoiseApp
//
//  Created by Karol Pilch on 27/10/2019.
//  Copyright © 2019 Karol Pilch. All rights reserved.
//

#ifndef ControlProcessor_hpp
#define ControlProcessor_hpp

#include <functional>
#include <map>
#include <string>

namespace na
{

/// Handles key strokes and provides command prompt.
class ControlProcessor
{
public:
    using Handler = std::function<std::string()>;

    /// Sets up the Quit and Help commands.
    ControlProcessor(const std::string& prompt = "");

    ~ControlProcessor();

    /// Register a handler for a key press.
    void registerCommand(char key, const std::string& help, const Handler& handler, bool caseSensitive = true);

    /// Set the new prompt text. The new prompt will replace the old one without making a new output line.
    void setPrompt(const std::string& prompt);

    /// Begins handling input. Thread is blocked until a quit command is selected.
    void handle();

    /// Prints the help text with the list of available commands.
    void printHelp() const;

private:
    struct HandlerInfo
    {
        Handler func;
        std::string helpText;
        bool caseSensitive;
    };

    void printPrompt() const;

    std::string _prompt;
    std::map<char, HandlerInfo> _handlers;
    bool _active {true};

};

}

#endif /* ControlProcessor_hpp */
