//
//  AudioEngine.cpp
//  NoiseApp
//
//  Created by Karol Pilch on 24/10/2019.
//  Copyright © 2019 Karol Pilch. All rights reserved.
//

#include "AudioEngine.hpp"
#include "AudioEngineTypes.hpp"

#include <chrono>
#include <iostream>
#include <iomanip>

namespace na
{

namespace portaudio
{

SampleCount buffer_count {0};

// Holds the currently processed buffer.
Buffer currentBuffer;

// The callback for PortAudio. Passes the raw data pointer to our
// structured buffer and initiates processing in AudioEngine.
int paCallback(const void* input,
               void* output,
               SampleCount frameCount,
               const PaStreamCallbackTimeInfo* timeInfo,
               PaStreamCallbackFlags statusFlags,
               void* userData)
{
#ifdef DEBUG_TIME
    using clock = std::chrono::high_resolution_clock;
    auto start = clock::now();
    buffer_count++;
    std::cout << "Buffer start: " << buffer_count << std::endl;
#endif
    Sample** fout = reinterpret_cast<Sample**>(output);
    currentBuffer.setRawData(fout, frameCount);
    reinterpret_cast<AudioEngine*>(userData)->process(currentBuffer);

#ifdef DEBUG_TIME
    std::chrono::duration<double> execution = clock::now() - start;

    double allocatedTime {timeInfo->outputBufferDacTime - timeInfo->currentTime};

    double slackPercent {100.0 * (allocatedTime - execution.count()) / allocatedTime};
    std::cout << "Time we have: " << std::fixed << std::setprecision(5) << allocatedTime << ", time we took: " << execution.count() << ", slack: " << slackPercent << ", output time: " << timeInfo->outputBufferDacTime << std::endl;
    std::cout << "Buffer end: " << buffer_count << std::endl;
#endif

    return 0;
}
}

AudioEngine::PortAudioError::PortAudioError(const char* what, PaError errorNumber)
 : std::runtime_error{what}
 , _errorNumber(errorNumber)
{
}

PaError AudioEngine::PortAudioError::getNumber() const
{
    return _errorNumber;
}

AudioEngine::AudioEngine() noexcept(false)
{
    auto err = Pa_Initialize();
    if (err != paNoError)
    {
        throw PortAudioError {"Failed to initialise PortAudio.", err};
    }

    _paStreamParams.channelCount = NUM_CHANNELS;
    _paStreamParams.device = Pa_GetDefaultOutputDevice();
    // Note: On my Mac OS 10.15 using interleaved format doesn't work.
    _paStreamParams.sampleFormat = paFloat32 | paNonInterleaved;
    _paStreamParams.suggestedLatency = 0.05;
    _paStreamParams.hostApiSpecificStreamInfo = nullptr;

    err = Pa_OpenStream(&_paStream,
                        nullptr,
                        &_paStreamParams,
                        FRAME_RATE,
                        paFramesPerBufferUnspecified,
                        paClipOff,
                        &portaudio::paCallback,
                        this);

    if (err != paNoError)
    {
        throw PortAudioError {"Failed to open PortAudio stream.", err};
    }
}

AudioEngine::~AudioEngine() noexcept(false)
{
    Pa_CloseStream(_paStream);
    Pa_Terminate();
}

void AudioEngine::start() noexcept(false)
{
    auto err = Pa_StartStream(_paStream);
    if (err != paNoError)
    {
        throw PortAudioError {"Failed to start audio stream.", err};
    }
}

void AudioEngine::stop() noexcept(false)
{
    if (!isPlaying())
    {
        return;
    }
    auto err = Pa_AbortStream(_paStream);
    if (err != paNoError)
    {
        throw PortAudioError {"Failed to stop audio stream.", err};
    }
}

void AudioEngine::toggle() noexcept(false)
{
    if (isPlaying())
    {
        stop();
    }
    else
    {
        start();
    }
}

bool AudioEngine::isPlaying() const
{
    return Pa_IsStreamActive(_paStream) == 1;
}

void AudioEngine::addProcessor(const std::shared_ptr<AudioProcessor> &processor)
{
    _processors.push_back(processor);
}

void AudioEngine::process(Buffer &audio)
{
    for (auto& processor : _processors)
    {
        if (processor->isEnabled())
        {
            processor->process(audio);
        }
    }
}

}
