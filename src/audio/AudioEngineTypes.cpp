//
//  AudioEngineTypes.cpp
//  NoiseApp
//
//  Created by Karol Pilch on 24/10/2019.
//  Copyright © 2019 Karol Pilch. All rights reserved.
//

#include "AudioEngineTypes.hpp"

namespace na
{

Buffer::Iterator::Iterator(Buffer* owner, size_t index)
: _currentIndex {index}
, _owner {owner}
{
    updateCurrentFrame();
}

Buffer::Frame& Buffer::Iterator::operator*()
{
    return _currentFrame;
}

Buffer::Iterator& Buffer::Iterator::operator++()
{
    ++_currentIndex;
    updateCurrentFrame();
    return *this;
}

 bool Buffer::Iterator::operator==(const Iterator &other) const
{
    return other._currentIndex == _currentIndex && other._owner == _owner;
}

Buffer::Iterator::operator bool() const
{
    return _owner && _currentIndex < _owner->_size;
}

void Buffer::Iterator::updateCurrentFrame()
{
    if (*this)
    {
        for (size_t channel {0}; channel < NUM_CHANNELS; ++channel)
        {
            _currentFrame[channel] = _owner->_rawSamples[channel] + _currentIndex;
        }
    }
}

void Buffer::setRawData(Sample **rawData, SampleCount length)
{
    _size = length;
    _rawSamples = rawData;
}

Buffer::Iterator Buffer::begin()
{
    return Iterator(this, 0);
}

Buffer::Iterator Buffer::end()
{
    return Iterator(this, _size);
}

SampleCount Buffer::size() const
{
    return _size;
}

bool AudioProcessor::isEnabled() const
{
    return _enabled;
}

void AudioProcessor::toggleEnabled()
{
    _enabled = !_enabled;
}

}
