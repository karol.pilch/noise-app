//
//  VolumeMeter.hpp
//  NoiseApp
//
//  Created by Karol Pilch on 27/10/2019.
//  Copyright © 2019 Karol Pilch. All rights reserved.
//

#ifndef VolumeMeter_hpp
#define VolumeMeter_hpp

#include "../AudioEngineTypes.hpp"

#include <atomic>
#include <array>
#include <shared_mutex>

namespace na
{

/// Measures the volume of the signal with 0.5s resolution.
class VolumeMeter: public AudioProcessor
{
public:
    /// Returns the maximum volume across all channels.
    Sample getMonoVolume() const;

    /// Returns the volume as an integer in range [0 ... scale].
    int getVolumeBars(int scale = 10) const;

    /// Returns the maximum volume of a particular channel.
    Sample getChannelVolume(size_t channelIndex) const;

private:
    /// Required override for processing samples.
    void process(Buffer& b) override;

    std::array<Sample, NUM_CHANNELS> _levels;
    mutable std::shared_mutex _levelsMutex; // Mutable so it can be locked in const getters.
    using SharedLock = std::shared_lock<std::shared_mutex>;
    using UniqueLock = std::unique_lock<std::shared_mutex>;

    unsigned long long _resetCounter {0};
};

}

#endif /* VolumeMeter_hpp */
