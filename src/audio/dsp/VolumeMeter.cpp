//
//  VolumeMeter.cpp
//  NoiseApp
//
//  Created by Karol Pilch on 27/10/2019.
//  Copyright © 2019 Karol Pilch. All rights reserved.
//

#include "VolumeMeter.hpp"

#include <cmath>

namespace na
{

namespace
{
/// The time in seconds between resetting the levels.
static constexpr double RESOLUTION {0.5};
static constexpr SampleCount RESOLUTION_SAMPLES {static_cast<SampleCount>(RESOLUTION * FRAME_RATE)};
}

Sample VolumeMeter::getMonoVolume() const
{
    Sample result {0.f};
    SharedLock l {_levelsMutex};
    for (const Sample& level : _levels)
    {
        result = std::fmax(level, result);
    }
    return result;
}

int VolumeMeter::getVolumeBars(int scale) const
{
    Sample scaled {getMonoVolume() * scale};
    return static_cast<int>(std::ceil(scaled));
}

Sample VolumeMeter::getChannelVolume(size_t channelIndex) const
{
    SharedLock l {_levelsMutex};
    return _levels[channelIndex];
}

void VolumeMeter::process(Buffer &buffer)
{
    std::array<Sample, NUM_CHANNELS> levels;
    {
        SharedLock l {_levelsMutex};
        levels = _levels;
    }

    for (auto& frame : buffer)
    {
        for (size_t c {0}; c < NUM_CHANNELS; ++c)
        {
            levels[c] = std::fmax(levels[c], std::fabs(*frame[c]));
        }

        ++_resetCounter;
        if (_resetCounter > RESOLUTION_SAMPLES)
        {
            for (Sample& l : levels)
            {
                l = 0.f;
            }
            _resetCounter = 0;
        }
    }

    UniqueLock l {_levelsMutex};
    _levels = levels;
}

}
