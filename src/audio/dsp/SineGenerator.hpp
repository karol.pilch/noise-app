//
//  SineGenerator.hpp
//  NoiseApp
//
//  Created by Karol Pilch on 29/10/2019.
//  Copyright © 2019 Karol Pilch. All rights reserved.
//

#ifndef SineGenerator_hpp
#define SineGenerator_hpp

#include "../AudioEngineTypes.hpp"

namespace na
{

// For now this is for debug purposes only.
class SineGenerator: public AudioProcessor
{
public:
    SineGenerator() { setFrequency(20.0); } // DEBUG
    void setFrequency(double f);

private:
    void process(Buffer& audio) override;

    double _phase {0.0};
    double _phaseStep;
    double _volume {0.2};

    double _currentFreq {20.0};
};

}

#endif /* SineGenerator_hpp */
