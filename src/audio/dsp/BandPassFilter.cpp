//
//  BandPassFilter.cpp
//  NoiseApp
//
//  Created by Karol Pilch on 28/10/2019.
//  Copyright © 2019 Karol Pilch. All rights reserved.
//

#include "BandPassFilter.hpp"

#include <cassert>
#include <cmath>
#include <sstream>
#include <tuple>

// #define DEBUG_BANDPASS

#ifdef DEBUG_BANDPASS
#include <iostream>
#include <iomanip>
#endif

namespace na
{

namespace
{
/// The amount of frequency steps available.
static constexpr int STEP_COUNT {20};

/// The predefined frequency steps for adjusting parameters in changeParams().
const std::array<BandPassFilter::Coeff, STEP_COUNT> FREQ_STEPS {
    20.0,
    40.0,
    90.0,
    180.0,
    330.0,
    560.0,
    880.0,
    1300.0,
    1840.0,
    2500.0,
    3300.0,
    4300.0,
    5500.0,
    6800.0,
    8500.0,
    10200.0,
    12000.0,
    14500.0,
    17000.0,
    20000.0
};

}

BandPassFilter::BandPassFilter()
{
	updateCoeffs();
    for (auto& channel : _input)
    {
        channel = std::vector<Sample> (ORDER - 1, 0.f);
    }
}

void BandPassFilter::setParams(const Params &params)
{
    _params = params;
    updateCoeffs();
}

void BandPassFilter::changeParams(int lowStepDelta, int highStepDelta)
{
    static constexpr size_t STEP {0};
    static constexpr size_t FOUND {1};
    std::tuple<int, bool> lowStepSearch {0, false};
    std::tuple<int, bool> highStepSearch {0, false};
    for (int step {0}; step < STEP_COUNT; ++step)
    {
        if (!std::get<FOUND>(lowStepSearch) && _params.lowFreq <= FREQ_STEPS[step])
        {
            std::get<STEP>(lowStepSearch) = step;
            std::get<FOUND>(lowStepSearch) = true;
        }
        if (!std::get<FOUND>(highStepSearch) && _params.highFreq <= FREQ_STEPS[step])
        {
            std::get<STEP>(highStepSearch) = step;
            std::get<FOUND>(highStepSearch) = true;
        }
        if (std::get<FOUND>(lowStepSearch) && std::get<FOUND>(highStepSearch))
        {
            break;
        }
    }

    int& lowStep {std::get<STEP>(lowStepSearch)};
    int& highStep {std::get<STEP>(highStepSearch)};

    lowStep = std::min(STEP_COUNT - 1, std::max(0, lowStep + lowStepDelta));
    highStep = std::min(STEP_COUNT - 1, std::max(0, highStep + highStepDelta));

    // Ensure low frequency is not higher than high frequency.
    if (lowStep > highStep)
    {
        if (lowStepDelta)
        {
            lowStep = highStep;
        }
        else
        {
            // Making an arbitrary decision here in case both deltas were non-0.
            highStep = lowStep;
        }
    }

    _params.lowFreq = FREQ_STEPS[lowStep];
    _params.highFreq = FREQ_STEPS[highStep];
    if (lowStep == highStep)
    {
        _params.highFreq *= 1.02f;
    }
    updateCoeffs();
}

BandPassFilter::Params BandPassFilter::getParams() const
{
    return _params;
}

std::string BandPassFilter::getParamsDescription() const
{
    std::stringstream result;
    result << "Filter HPF: " << _params.lowFreq << " Hz, LPF: " << _params.highFreq << " Hz";
    return result.str();
}

void BandPassFilter::process(Buffer& audio)
{
    // Stolen from http://digitalsoundandmusic.com/7-3-1-convolution-and-time-domain-filtering/

    static constexpr int PREV_SAMPLE_COUNT {ORDER - 1};
    Sample inVal;
    Coeffs coeffs;
    {
        std::shared_lock<std::shared_mutex> l {_coeffsMutex};
        coeffs = _coeffs;
    }

    for (auto& channel : _input)
    {
        channel.resize(audio.size() + PREV_SAMPLE_COUNT);
    }

    assert(audio.size() > ORDER); // Can't handle buffers so small in this implementation.

#ifdef DEBUG_BANDPASS
    std::streamsize osize {std::cout.precision()};
    std::streamsize owidth {std::cout.width()};
    std::cout << std::setprecision(3) << '<';
    for (size_t i {0}; i <= PREV_SAMPLE_COUNT; ++i)
    {
        std::cout << ' ' << std::setw(5) << i << ':' << std::setw(6) << _input[0][i];
    }
    std::cout << std::endl;
#endif

    size_t c;
    size_t f {0};
    for (auto& frame : audio)
    {
        c = 0;
        for (auto sample : frame)
        {
            _input[c][f + PREV_SAMPLE_COUNT] = *sample;

            *sample = 0.f;
            for (int k {0}; k < ORDER; ++k)
            {

                inVal = _input[c][f - k + PREV_SAMPLE_COUNT];
                *sample += static_cast<Sample>(coeffs[k] * inVal);
            }
            ++c;
        }
        ++f;
    }

    for (auto& channel : _input)
    {
        // Save the end of the current input to the beginning of the next since it will be needed in the next call.
        std::copy(channel.cend() - PREV_SAMPLE_COUNT - 1, channel.cend(), channel.begin());
    }

#ifdef DEBUG_BANDPASS
    std::cout << '>';
    for (size_t i {_input[0].size() - PREV_SAMPLE_COUNT - 1}; i < _input[0].size(); ++i)
    {
        std::cout << ' ' << std::setw(5) << i << ':' << std::setw(6) << _input[0][i];
    }
    std::cout << std::setprecision(static_cast<int>(osize)) << std::setw(static_cast<int>(owidth)) << std::endl;
#endif
}

void BandPassFilter::updateCoeffs()
{
    // Algo stolen from http://digitalsoundandmusic.com/7-3-2-low-pass-high-pass-bandpass-and-bandstop-filters/
    Coeff lowFreqC;
    Coeff highFreqC;
    {
        lowFreqC = _params.lowFreq / FRAME_RATE;
        highFreqC = _params.highFreq / FRAME_RATE;
    }

    Coeff wLowC {2.0 * M_PI * lowFreqC};
    Coeff wHighC {2.0 * M_PI * highFreqC};

    int middle = ORDER / 2;

    Coeffs newCoeffs;
    for (int i {ORDER / -2}; i <= ORDER / 2; ++i)
    {
        if (i == 0)
        {
            newCoeffs[middle] = 2 * (highFreqC - lowFreqC);
        }
        else
        {
            newCoeffs[i + middle] =
                sin(wHighC * i) / (M_PI * i) -
                sin(wLowC  * i) / (M_PI * i);
        }
    }

    std::unique_lock<std::shared_mutex> l {_coeffsMutex};
    _coeffs = newCoeffs;
}

}
