//
//  NoiseGenerator.hpp
//  NoiseApp
//
//  Created by Karol Pilch on 26/10/2019.
//  Copyright © 2019 Karol Pilch. All rights reserved.
//

#ifndef NoiseGenerator_hpp
#define NoiseGenerator_hpp

#include "../AudioEngineTypes.hpp"

#include <algorithm>
#include <atomic>
#include <random>

namespace na
{


/// A simple noise generator.
class NoiseGenerator: public AudioProcessor
{
public:
    /// The seed for the random engine.
    static constexpr std::default_random_engine::result_type SEED {10};

    /// Sets the volume in range [0.0, 1.0]. Values outside of this range will be clipped.
    void setVolume(Sample volume);

    /// Modifies the volume by the delta. Uses `setVolume()` internally.
    void changeVolume(Sample delta);

    Sample getVolume() const { return _volume; }

private:
    /// The required implementation. Fills the buffer with noise (same value in each channel).
    void process(Buffer& audio) override;

    std::default_random_engine _random_engine {SEED};
    std::atomic<Sample> _volume {0.f};
};

}

#endif /* NoiseGenerator_hpp */
