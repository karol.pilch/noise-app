//
//  Limiter.cpp
//  NoiseApp
//
//  Created by Karol Pilch on 06/11/2019.
//  Copyright © 2019 Karol Pilch. All rights reserved.
//

#include "Limiter.hpp"

namespace na
{

bool Limiter::checkActive()
{
    const bool result {_active};
    _active = false;
    return result;
}

bool Limiter::checkClipping()
{
    const bool result {_clipping};
    _clipping = false;
    return result;
}

void Limiter::process(Buffer& audio)
{
    // TODO HERE
    // TODO: Implement the suggestion from https://dsp.stackexchange.com/a/1396
}

}