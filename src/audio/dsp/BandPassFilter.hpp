//
//  BandPassFilter.hpp
//  NoiseApp
//
//  Created by Karol Pilch on 28/10/2019.
//  Copyright © 2019 Karol Pilch. All rights reserved.
//

#ifndef BandPassFilter_hpp
#define BandPassFilter_hpp

#include "../AudioEngineTypes.hpp"

#include <array>
#include <shared_mutex>
#include <string>
#include <vector>

namespace na
{

/// A FIR band-pass filter.
class BandPassFilter: public AudioProcessor
{
public:
    using Coeff = double;
    struct Params
    {
        Coeff lowFreq {20.0};
        Coeff highFreq {20000.0};
    };

    BandPassFilter();

    /// Sets the high and low frequency.
    void setParams(const Params& params);

    /// Moves the parameters in steps as specified in `FREQ_STEPS`.
    void changeParams(int lowStepDelta, int highStepDelta);

    /// Returns the currently used parameters.
    Params getParams() const;

    /// Returns a string describing the currently used parameters.
    std::string getParamsDescription() const;


private:
    void process(Buffer& audio) override;

    static constexpr int ORDER {21};

    using Coeffs = std::array<Coeff, ORDER>;

    /// Updates the filter coefficients based on the (probably updated) params.
    void updateCoeffs();

    /// The currently used parameters.
    Params _params;

    /// The filter coefficients. Used in both audio and control threads, hence the mutex.
    Coeffs _coeffs;
    mutable std::shared_mutex _coeffsMutex;

    /// Holds the copy of the buffer being processed, including the last few frames from the previous buffer that are
    /// needed for convolution.
	std::array<std::vector<Sample>, NUM_CHANNELS> _input;

};

}

#endif /* BandPassFilter_hpp */
