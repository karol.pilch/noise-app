//
//  NoiseGenerator.cpp
//  NoiseApp
//
//  Created by Karol Pilch on 26/10/2019.
//  Copyright © 2019 Karol Pilch. All rights reserved.
//

#include "NoiseGenerator.hpp"

#include <iostream>

#include "../AudioEngine.hpp"

namespace na
{

namespace
{
std::uniform_real_distribution<Sample> DISTRIBUTION{-1.f, 1.f};
}

void NoiseGenerator::setVolume(Sample volume)
{
#ifdef DEBUG_TIME
    std::cout << "At buffer: " << portaudio::buffer_count << " volume set to " << volume << std::endl;
#endif
    _volume = std::max(0.f, std::min(1.f, volume));
}

void NoiseGenerator::changeVolume(Sample delta)
{
    setVolume(_volume + delta);
}

void NoiseGenerator::process(Buffer& audio)
{
    Sample volume {_volume.load()};
#ifdef DEBUG_TIME
    std::cout << "Noise generator at buffer: " << portaudio::buffer_count << " volume: " << volume << std::endl;
#endif
    float sampleValue;
    for (auto& frame : audio)
    {
        sampleValue = DISTRIBUTION(_random_engine) * volume;
        for (Sample* s : frame)
        {
            *s = sampleValue;
        }
    }
}

}
