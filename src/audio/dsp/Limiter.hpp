//
//  Limiter.hpp
//  NoiseApp
//
//  Created by Karol Pilch on 06/11/2019.
//  Copyright © 2019 Karol Pilch. All rights reserved.
//

#ifndef Limiter_hpp
#define Limiter_hpp

#include "../AudioEngineTypes.hpp"

#include <atomic>

namespace na
{

/// A simple limiter ensuring the signal doesn't hard-clip
class Limiter : public AudioProcessor
{
public:
    /// Checks whether the limiter has been active since the last check.
    bool checkActive();

    /// Checks whether the limiter has clipped since the last check.
    bool checkClipping();

private:
    void process(Buffer& audio) override;

    std::atomic_bool _active;
    std::atomic_bool _clipping;
};

}

#endif /* Limiter_hpp */
