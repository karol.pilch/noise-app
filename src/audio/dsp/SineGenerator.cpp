//
//  SineGenerator.cpp
//  NoiseApp
//
//  Created by Karol Pilch on 29/10/2019.
//  Copyright © 2019 Karol Pilch. All rights reserved.
//

#include "SineGenerator.hpp"

#include "../AudioEngine.hpp"

#include <cmath>

namespace na
{

namespace
{
double constexpr TWO_PI {2.0 * M_PI};
}

void SineGenerator::setFrequency(double f)
{
    _phaseStep = TWO_PI * f / FRAME_RATE;
    _currentFreq = f;
}

void SineGenerator::process(Buffer &audio)
{
    float sampleValue;
    for (auto& frame : audio)
    {
        sampleValue = sin(_phase) * _volume;
        _phase += _phaseStep;
        for (Sample* s : frame)
        {
            *s = sampleValue;
        }
    }

    _phase = fmod(_phase, TWO_PI);

    // DEBUG
    if (portaudio::buffer_count % 2 == 0)
    {
        double nextFreq = _currentFreq + (_currentFreq > 5000.0 ? 100.0 : 20.0);
        if (nextFreq > 20000.0)
        {
            nextFreq = 20.0;
        }
        setFrequency(nextFreq);
    }
}
}
