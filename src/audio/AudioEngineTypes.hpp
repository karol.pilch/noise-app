//
//  AudioProcessor.hpp
//  NoiseApp
//
//  Created by Karol Pilch on 24/10/2019.
//  Copyright © 2019 Karol Pilch. All rights reserved.
//

#ifndef AudioEngineTypes_hpp
#define AudioEngineTypes_hpp

#include <array>
#include <atomic>
#include <vector>

namespace na
{

/// The number of channels our AudioEngine is handling.
static constexpr size_t NUM_CHANNELS{2};

/// The sampling frequency.
static constexpr double FRAME_RATE{48000.0};

using Sample = float;
using SampleCount = unsigned long;

class AudioEngine;

/// A single multi-channel audio buffer.
///
/// A buffer's most important function is to provide a STL-style iterator over the raw audio data.
class Buffer
{
public:
    using Frame = std::array<Sample*, NUM_CHANNELS>;

    /// Deferences to a `Frame` object which contains pointers to sample values for all channels at the current time
    /// point.
    struct Iterator
    {
        Iterator(Buffer* owner, size_t index);

        Frame& operator*();
        Iterator& operator++();
        bool operator==(const Iterator& other) const;
        operator bool() const;

    private:
        void updateCurrentFrame();

        size_t _currentIndex {0};
        Frame _currentFrame;
        Buffer* _owner;
    };

    /// Updates the pointer to the audio buffer that backs this Buffer.
    void setRawData(Sample** rawData, SampleCount length);

    Iterator begin();
    Iterator end();

    SampleCount size() const;

private:
    SampleCount _size;
    Sample** _rawSamples;
};

/// The interface for DSP effects used by `AudioEngine`.
class AudioProcessor
{
public:

    /// Returns `true` when this Processor is enabled, or `false` otherwise.
    bool isEnabled() const;

    /// Toggles the enabled state of this processor.
    void toggleEnabled();

private:
    friend class AudioEngine; // so that it can process()

    /// The heart of the processor. Override this to change the samples in the buffer.
    virtual void process(Buffer& audio) = 0;

private:
    std::atomic_bool _enabled {true};
};

}

#endif
