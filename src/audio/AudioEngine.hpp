//
//  AudioEngine.hpp
//  NoiseApp
//
//  Created by Karol Pilch on 24/10/2019.
//  Copyright © 2019 Karol Pilch. All rights reserved.
//

#ifndef AudioEngine_hpp
#define AudioEngine_hpp

#include "AudioEngineTypes.hpp"

#include <portaudio.h>

#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

//#define DEBUG_TIME

namespace na
{

namespace portaudio
{
extern SampleCount buffer_count;

int paCallback(const void* input,
               void* output,
               SampleCount frameCount,
               const PaStreamCallbackTimeInfo* timeInfo,
               PaStreamCallbackFlags statusFlags,
               void* userData);
}

/// Provides an interface between our code and OS audio.
class AudioEngine
{
public:

    /// Used when something goes wrong in PortAudio.
    class PortAudioError : public std::runtime_error
    {
    public:
        PortAudioError(const char* what, PaError errorNumber);
        PaError getNumber() const;
    private:
        PaError _errorNumber;
    };

    AudioEngine() noexcept(false);
    ~AudioEngine() noexcept(false);

    /// Starts the audio processing and output.
    void start() noexcept(false);

    /// Stops the audio processing. Called automatically on destruction.
    void stop() noexcept(false);

    /// Toggles between start and stop.
    void toggle() noexcept(false);

    /// Returns `true` if the engine is outputting sound, or `false` otherwise.
    bool isPlaying() const;

    /// Adds a processor to the chain of processors.
    ///
    /// Takes effect without smoothing after the current buffer is processed.
    void addProcessor(const std::shared_ptr<AudioProcessor>& processor);

private:
    friend int portaudio::paCallback(const void*, void*, SampleCount , const PaStreamCallbackTimeInfo*, PaStreamCallbackFlags, void*);
    
    /// To be called by PA callback function.
    void process(Buffer& audio);

    PaStream* _paStream {nullptr};
    PaStreamParameters _paStreamParams;

    std::vector<std::shared_ptr<AudioProcessor>> _processors;
};

}

#endif /* AudioEngine_hpp */
