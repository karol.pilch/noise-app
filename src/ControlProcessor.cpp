//
//  ControlProcessor.cpp
//  NoiseApp
//
//  Created by Karol Pilch on 27/10/2019.
//  Copyright © 2019 Karol Pilch. All rights reserved.
//

#include "ControlProcessor.hpp"

#include <cassert>
#include <iostream>
#include <thread>

namespace na
{

namespace
{
static const std::string PROMPT_TERMINATOR{": >"};
static const std::string RESPONSE_PREFIX{" "};
}

ControlProcessor::ControlProcessor(const std::string& prompt)
: _prompt {prompt}
{
    registerCommand('q', "Stop and quit", [this] {
        _active = false;
        return "Quitting.";
    }, false);
    registerCommand('h', "Help", [this] {
        printHelp();
        return "Helping.";
    }, false);

    // Note: this only works on OS X AFAIK.
    // On Windows you have to find another method of getting characters staright away without waiting for return key.
    system("stty raw");
}

ControlProcessor::~ControlProcessor()
{
    // See note in constructor.
    system("stty cooked");
}

void ControlProcessor::registerCommand(char key, const std::string &help, const Handler &handler, bool caseSensitive)
{
    if (!caseSensitive)
    {
        key = tolower(key);
    }
    assert(_handlers.count(key) == 0);
    _handlers.emplace(key, HandlerInfo{handler, help, caseSensitive});
}

void ControlProcessor::setPrompt(const std::string &prompt)
{
    _prompt = prompt;
    printPrompt();
}

void ControlProcessor::printHelp() const
{
    std::cout << std::endl << "\r" << "Usage:" << std::endl << "\r"
        << "Press the specified key to perform described action:" << std::endl << "\r";
    for (const auto& el : _handlers)
    {
        if (el.second.caseSensitive)
        {
            std::cout << el.first << "  ";
        }
        else
        {
            std::cout << el.first << '|' << static_cast<char>(toupper(el.first));
        }
        std::cout << " - " << el.second.helpText << std::endl << "\r";
    }
    printPrompt();
}

void ControlProcessor::printPrompt() const
{
    int flushChars {60};
    std::cout << "\r";
    while (flushChars-- >= 0)
    {
        std::cout << " ";
    }
    std::cout << "\r" << _prompt << PROMPT_TERMINATOR << std::flush;
}

void ControlProcessor::handle()
{
    static constexpr char CTRL_C{3};
    printPrompt();
    while(_active)
    {
        char command = std::cin.get();
        char lCommand = tolower(command);

        if (command == CTRL_C)
        {
            break;
        }

        if (!std::cin) {
            // std::cin does EOF endlessly after the program receives signal from debugger. To prevent flooding the
            // console with 'unrecognised command' messages, we try to clear here. This probably isn't desirable in
            // production.
            std::cin.clear();

            if (!std::cin)
            {
                // Clearing didn't work so we can't receive any commands.
                break;
            }
            else
            {
                continue;
            }
        }

        auto handler = _handlers.find(command);
        if (handler == _handlers.end())
        {
            handler = _handlers.find(lCommand);
            if (handler == _handlers.end() || handler->second.caseSensitive)
            {
                std::cout << RESPONSE_PREFIX << "Unrecognised command '" << command << "' (" << static_cast<int>(command) << ").";
                continue;
            }
        }

        const std::string response {handler->second.func()};
        printPrompt();
        std::cout << RESPONSE_PREFIX << response;
    }
    std::cout << std::endl << "\r";
}

}
